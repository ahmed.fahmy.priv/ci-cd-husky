import { render, fireEvent, screen } from "@testing-library/react";
import Button from "./Button";

const handleClick = jest.fn();

describe("Button snapshot test", () => {
  test("should button render with no errors", () => {
    const { asFragment } = render(<Button children="test button" className="test class" />);

    expect(asFragment()).toMatchSnapshot();
  });
});

describe("button function words", () => {
  test("should button onclick function work", () => {
    render(<Button children="test button" className="test class" handleClick={handleClick} />);

    fireEvent.click(screen.getByText("test button"));

    expect(handleClick).toHaveBeenCalled();
  });
});
