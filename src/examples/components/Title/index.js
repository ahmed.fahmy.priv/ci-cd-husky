import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

const Title = ({ title, className, handleClick }) => {
  const [isParagraph, setIsParagraph] = useState(false);

  return (
    <>
      <div data-testid="container" onClick={handleClick}>
        <h1 className={className}>{title}</h1>
        {isParagraph && <p>paragraph</p>}
        <button onClick={() => setIsParagraph((prev) => !prev)}>BUTTON</button>
      </div>
    </>
  );
};

Title.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
};

Title.defaultProps = {
  title: "Hello",
  className: "",
};

export default Title;
