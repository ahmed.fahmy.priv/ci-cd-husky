import Title from "./index";
import { render, screen, fireEvent } from "@testing-library/react";

// describe("title rendering", () => {
//   test("should title render with no errors", () => {
//     const { asFragment } = render(<Title title="test title" className="test class" />);
//     expect(asFragment()).toMatchSnapshot();
//   });
// });

const handleClick = jest.fn();

describe("Title button works", () => {
  test("should container function work", () => {
    render(<Title title="test title" className="test class" handleClick={handleClick} />);
    fireEvent.click(screen.getByTestId("container"));
    expect(handleClick).toHaveBeenCalled();
  });
});
