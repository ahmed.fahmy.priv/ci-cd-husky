import Modal from "./Modal";
import { fireEvent, render, screen } from "@testing-library/react";
import { useState } from "react";

const handleYesClick = jest.fn();

const ModalWrapper = () => {
  const [isOpen, setIsOpen] = useState(true);

  return (
    <>
      <Modal isOpen={isOpen} setIsOpen={setIsOpen} handleYesClick={handleYesClick} />
      <button
        onClick={() => {
          setIsOpen((prev) => !prev);
        }}
      >
        Button
      </button>
    </>
  );
};

// describe("Modal Snapshot testing", () => {
//   test("should Modal not open when isOpen is false", () => {
//     const { asFragment } = render(<Modal isOpen={false} />);
//     expect(asFragment()).toMatchSnapshot();
//   });

//   test("should Modal open when isOpen is true", () => {
//     const { asFragment } = render(<Modal isOpen={true} title="test title" />);
//     expect(asFragment()).toMatchSnapshot();
//   });
// });

// describe("Modal yes button works", () => {
//   test("should modal yes button handleclick be called", () => {
//     render(<Modal title="test title" isOpen={true} handleYesClick={handleYesClick} />);

//     fireEvent.click(screen.getByText("Yes"));

//     expect(handleYesClick).toHaveBeenCalled();
//   });
// });

describe("close modal functionality works", () => {
  test("should close modal X button work", () => {
    render(<ModalWrapper />);
    expect(screen.getByTestId("modal")).toBeInTheDocument();

    fireEvent.click(screen.getByText("X"));
    expect(screen.queryByTestId("modal")).not.toBeInTheDocument();
  });

  test("should click on modal bg close modal", () => {
    render(<ModalWrapper />);

    expect(screen.getByTestId("modal")).toBeInTheDocument();

    fireEvent.click(screen.getByTestId("modal-bg"));
    expect(screen.queryByTestId("modal")).not.toBeInTheDocument();
  });
});
