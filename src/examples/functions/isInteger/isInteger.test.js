import isInteger from "./index";

describe("is integer function work", () => {
  test("should return true if integer argument is given", () => {
    expect(isInteger(5)).toBeTruthy();
  });

  test("should return false if integer not given in argument", () => {
    expect(isInteger("sdf")).toBeFalsy();
  });
});
