import sum from "../sum/index";

describe("sum work", () => {
  test("should add 2 arguments", () => {
    expect(sum(3, 4)).toBe(7);
  });

  test("should return null", () => {
    expect(sum(5)).toBe(null);
  });
});
