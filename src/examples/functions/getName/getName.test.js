import getName from "./index";

const additionalCallback = jest.fn();

describe("getName fn work", () => {
  test("should return john if arguments are not given", () => {
    expect(getName()).toBe("John");
  });

  test("should namechanger fn work", () => {
    const capitalizeFn = (value) => {
      return value.toUpperCase();
    };
    expect(getName(capitalizeFn)).toBe("JOHN");
  });
});

describe("additional call back called", () => {
  test("should additonal callback be called", () => {
    getName(null, additionalCallback);
    expect(additionalCallback).toHaveBeenCalled();
  });
});
